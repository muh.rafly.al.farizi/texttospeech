package net.gotev.speechdemo;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import net.gotev.speech.GoogleVoiceTypingDisabledException;
import net.gotev.speech.Speech;
import net.gotev.speech.SpeechDelegate;
import net.gotev.speech.SpeechRecognitionNotAvailable;
import net.gotev.speech.SpeechUtil;
import net.gotev.speech.TextToSpeechCallback;
import net.gotev.speech.ui.SpeechProgressView;
import net.gotev.toyproject.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity implements SpeechDelegate {

    private final int PERMISSIONS_REQUEST = 1;

    private ImageButton button;
    private Button speak;
    private TextView text;
    private EditText textToSpeech;
    private SpeechProgressView progress;
    private LinearLayout linearLayout;
    private Spinner spinner;

    MediaPlayer mediaPlayer;

    Timer timerVoiceCommand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onMediaPlayer();

        Speech.init(this, getPackageName());

        linearLayout = findViewById(R.id.linearLayout);
        linearLayout.setOnClickListener(view -> onStopVoiceRun());

        button = findViewById(R.id.button);
        button.setOnClickListener(view -> onButtonClick());

        speak = findViewById(R.id.speak);
        speak.setOnClickListener(view -> onSpeakClick());

        text = findViewById(R.id.text);
        textToSpeech = findViewById(R.id.textToSpeech) ;
        progress = findViewById(R.id.progress);

        int[] colors = {
                ContextCompat.getColor(this, android.R.color.black),
                ContextCompat.getColor(this, android.R.color.darker_gray),
                ContextCompat.getColor(this, android.R.color.black),
                ContextCompat.getColor(this, android.R.color.holo_orange_dark),
                ContextCompat.getColor(this, android.R.color.holo_red_dark)
        };
        progress.setColors(colors);

        List<String> spinnerArray = new ArrayList<String>();
        spinnerArray.add("ENGLISH");
        spinnerArray.add("INDONESIA");
        spinnerArray.add("JAPAN");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner = findViewById(R.id.spinner);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectLanguage = spinnerArray.get(position);
                if (selectLanguage.equalsIgnoreCase("ENGLISH")) {
                    Speech.getInstance().setLocale(Locale.ENGLISH);

                } else if (selectLanguage.equalsIgnoreCase("INDONESIA")) {
                    Locale LOCAL_INDONESIA = new Locale("id", "ID");
                    Speech.getInstance().setLocale(LOCAL_INDONESIA);

                } else if (selectLanguage.equalsIgnoreCase("JAPAN")) {
                    Speech.getInstance().setLocale(Locale.JAPANESE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void onMediaPlayer() {

        mediaPlayer = MediaPlayer.create(this, R.raw.lily);
    }

    private void mediaPlayerOn() {
        mediaPlayer.start();
    }

    private void mediaPlayerOff() {
        if (mediaPlayer.isPlaying() || mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
            Log.e("MainActivity", "mediaPlayerOff: Media Player Rejected" );
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Speech.getInstance().shutdown();
    }

    private void onVoiceRun() {
        timerVoiceCommand = new Timer();
        timerVoiceCommand.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                  onRecordAudioPermissionGranted();
                    }
                });
            }
        }, 0,5000);
    }

    private void onStopVoiceRun() {
        if (timerVoiceCommand != null) {
            timerVoiceCommand.cancel();
        }
    }

    private void onButtonClick() {

        if (Speech.getInstance().isListening()) {
            Speech.getInstance().stopListening();
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
//                onRecordAudioPermissionGranted();
                onVoiceRun();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_REQUEST);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e("MainActivity", "onRequestPermissionsResult: ===== Start ====" );
        if (requestCode != PERMISSIONS_REQUEST) {
            Log.e("MainActivity", "onRequestPermissionsResult: " + PERMISSIONS_REQUEST );
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay!
                onRecordAudioPermissionGranted();
            } else {
                // permission denied, boo!
                Toast.makeText(MainActivity.this, R.string.permission_required, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void onRecordAudioPermissionGranted() {
        Log.e("MainActivity", "onRecordAudioPermissionGranted: ===== Start ====" );
        button.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);

        try {
            Speech.getInstance().stopTextToSpeech();
            Speech.getInstance().startListening(progress, MainActivity.this);

        } catch (SpeechRecognitionNotAvailable exc) {
            showSpeechNotSupportedDialog();

        } catch (GoogleVoiceTypingDisabledException exc) {
            showEnableGoogleVoiceTyping();
        }
    }

    private void onSpeakClick() {
//        final MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.lily);

        if (textToSpeech.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, R.string.input_something, Toast.LENGTH_LONG).show();
            return;
        } else if (textToSpeech.getText().toString().equals("play dance")) {
            Speech.getInstance().say("ok play music");
            mediaPlayerOn();

        } else if (textToSpeech.getText().toString().equals("stop dance")) {
            Speech.getInstance().say("stop music");
            if (mediaPlayer.isPlaying()) {
            mediaPlayerOff();
            }
        }

        Speech.getInstance().say(textToSpeech.getText().toString().trim(), new TextToSpeechCallback() {
            @Override
            public void onStart() {
                Toast.makeText(MainActivity.this, "TTS onStart", Toast.LENGTH_SHORT).show();
                onStopVoiceRun();
            }

            @Override
            public void onCompleted() {
                Toast.makeText(MainActivity.this, "TTS onCompleted", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError() {
                Toast.makeText(MainActivity.this, "TTS onError", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onStartOfSpeech() {
//        Speech.getInstance().isListening();
    }

    @Override
    public void onSpeechRmsChanged(float value) {
        //Log.d(getClass().getSimpleName(), "Speech recognition rms is now " + value +  "dB");
    }

    @Override
    public void onSpeechResult(String result) {

//       onMediaPlayer();

        Log.e("MainActivity", "onSpeechResult: ===== Start ====" );

        button.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.GONE);

        text.setText(result);

        if (result.isEmpty()) {
//            Speech.getInstance().say(getString(R.string.repeat));
            onVoiceRun();
//            timerVoiceCommand.cancel();

        } else if (result.equals(getString(R.string.hello)) || (result.equals("halo"))) {
            Speech.getInstance().say(getString(R.string.hai));

        } else if (result.equals(getString(R.string.how_are_you))) {
            Speech.getInstance().say(getString(R.string.im_fine_thank_youuu_and_you));

        } else if (result.equals(getString(R.string.im_very_well_thanks))) {
            Speech.getInstance().say(getString(R.string.what_are_you_doing_now));

        } else if (result.equals(getString(R.string.coding))) {
            Speech.getInstance().say(getString(R.string.wow_you_are_programmer));

        } else if (result.equals(getString(R.string.ya_of_cource))) {
            Speech.getInstance().say(getString(R.string.great_ok_go_a_head));
            onStopVoiceRun();
//            timerVoiceCommand.cancel();

        } else if (result.equals("halo") || (result.equals("hai"))) {
            Speech.getInstance().say(getString(R.string.hai));

        } else if (result.equals("apa kabar")) {
            Speech.getInstance().say("aku baik kalau kamu");

        } else if (result.equals("aku juga baik")) {
            Speech.getInstance().say("kamu lagi sibuk apa");

        } else if (result.equals("coding")) {
            Speech.getInstance().say("wah kamu programmer");

        } else if (result.equals("ya")) {
            Speech.getInstance().say("ok silahkan di lanjutkan");
            onStopVoiceRun();
//            timerVoiceCommand.cancel();

        } else if (result.equals("play Dance")) {
            Speech.getInstance().say("ok play music");
            onStopVoiceRun();
            mediaPlayerOn();

        } else if (result.equals("stop dance")) {
            Speech.getInstance().say("stop music");
            onStopVoiceRun();
            mediaPlayerOff();


        } else {
            Speech.getInstance().say(result);
        }

    }



    @Override
    public void onSpeechPartialResults(List<String> results) {
        text.setText("");
        for (String partial : results) {
            text.append(partial + " ");
        }
    }

    private void showSpeechNotSupportedDialog() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        SpeechUtil.redirectUserToGoogleAppOnPlayStore(MainActivity.this);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.speech_not_available)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, dialogClickListener)
                .setNegativeButton(R.string.no, dialogClickListener)
                .show();
    }

    private void showEnableGoogleVoiceTyping() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.enable_google_voice_typing)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // do nothing
                    }
                })
                .show();
    }

}
